# test-demo-poc-flowchart
This is a UiPath flowchart process.

## Description
This flowchart will go through different paths and log different outputs based on the number that is inputted into the flowchart.

## Requirements
This automation was built with UiPath Studio 2021.10.5, so for some of its activities and packages to run in UiPath Studio, it may require a version that is not lower than this.